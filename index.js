//console.log("Hello, 204!");

//Functions
	/*
		Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoked.

		Functions are mostly created to create complicated tasks to run several lines of codes.

	*/

//Function Declarations
	//(function statement) defines a function with a specified parameters

	/*
		Syntax:
			function functionName() {
					code block(statement);
			}

		function keyword - used ti defined a javascript functions
		functionName - the function name. Functions are named to be able to use later in the code.
		function block ({}) - the statement which comprise the body of the function. This where the code to be executed.
	*/


	function printName() {
		console.log("My name is John!");
	};

//Function Invocation
	/*
	
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.

	*/

	//Let's invoke/call the functions that we declared.
	printName();

	//declaredFunction(); // error - we cannot invoke a function we have yet to define


//Function Declarations vs Function Expression
	//Function Declarations

	//A function can be created through function declaration by using the function keyword and adding a function name.

	//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
			
	declaredFunction(); //declared functions can be hoisted. As long as the function has been declared

	//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.


	function declaredFunction() {
		console.log("Hello, from the declaredFunction()");
	};

	declaredFunction();



	//Function Expression
		//A function can be also stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

		//variableFunction(); Cannot access 'variableFunction' before initialization
		//error - function expressions, being stored in a let or const variable, cannot be hoisted.


		let variableFunction = function() {

			console.log("Hello Again!");
		}

		variableFunction();

		// We can also create a function expression of a named function.
		// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
		// Function Expressions are always invoked (called) using the variable name.


		let funcExpression = function funcName() {
			console.log("Hello from the other side!");
		}

		funcExpression();
		//funcName(); //error


	//You can reassign declared function and function expression to new anonymous function
		declaredFunction = function() {
			console.log("Updated declaredFunction");

		}

		declaredFunction();


		funcExpression = function() {
			console.log("Updated funcExpression");
		}

		funcExpression();

	//However, we cannot re-assign a function expression initialized with const.

	const constantFunc = function() {
		console.log("Initialized with const");
	}

	constantFunc();

	// constantFunc = function() {
	// 	console.log("Can we reassigned?");
	// }

	// constantFunc();


//Function Scoping
/*
	Scope is the accessibility (visibility) of variables within our program.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope

*/

	{
		let localVar = "Juan Dela Cruz";
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	//console.log(localVar);

//Function Scope
/*		
	JavaScript has function scope: Each function creates a new scope.
	Variables defined inside a function are not accessible (visible) from outside the function.
	Variables declared with var, let and const are quite similar when declared inside a function.
*/


	function showNames() {

		var functionVar = "Mark";
		const functionConst = "Jayson";
		let functionLet = "Arnel";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

// Nested Functions

	function myNewFunction() {
		let name = "Gabriella";

		function nestedFunction() {
			let nestedName = "Maria";
			console.log(name);
		}

		nestedFunction();
	}

	myNewFunction();


// Function and Global Scope Variables

	// Global Scope Variable
	let globalName = "Alexandro";

	function myNewFunction2() {
		let nameInside = "Khabib";

		console.log(globalName);

	}

	myNewFunction2();

// Using Alert()

// alert() allows us to show a small window at the top of our browser page to show information to our user.

alert("Hello, Javascript!");

function showSampleAlert() {
	alert("Hello, User!");
}

showSampleAlert();

console.log("Sample Message");

// Using prompt
	// prompt allows us to show a small window at the top of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from prompt() will be returned as a String once the user dismisses the window.
/*
	Syntax:
		prompt("<dialogInStrings>");

*/

let samplePrompt = prompt("Enter your name.");

console.log("Hello," +samplePrompt);


function printWelcomeMessage() {

	let firstName = prompt("Enter Your First Name.");
	let lastName = prompt("Enter Your Last Name.");

	console.log("Hello, " + firstName + " " +lastName+ "!");
	alert("Welcome to my page!");

}

printWelcomeMessage();

// Function Naming Conventions

	function getCourses() {

		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourses();

	// Avoid Generice names to avoid confusion within your code

	// Avoid pointless and inapropriate function names

	function foo () {
		console.log(25%5);
	}

	foo();

	// Name your functions in small caps. Follow the camelCase when naming variables and functions

	function displayCarInfo() {

		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();